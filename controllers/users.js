var User = require('../models/users');

exports.postUsers = function(req, res) {
    var user = new User({
        username: req.body.username,
        email: req.body.email,
        password: req.body.password,
        firstname: req.body.firstname,
        lastname: req.body.lastname,
    });

    user.save(function(err) {
        if (err)
        res.send(err);

        res.json({ message: 'User insert!', data : user });
    });
};

// Create endpoint /api/users for GET
exports.getUsers = function(req, res) {
    User.find(function(err, users) {
        if (err)
        res.send(err);

        res.json(users);
    });
};