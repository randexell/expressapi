var Product = require('../models/product');

exports.postProduct = function(req, res) 
{
    var product = new Product();
    product.name = req.body.name;
    product.type = req.body.type;
    product.value = req.body.value;
    product.quantity = req.body.quantity;

    product.save(function(err) 
    {
        if (err)
            res.send(err);

        res.json({ message: 'Product added to the storage!', data: product });
    });

};

// View all product
exports.getProduct = function(req, res) {

    Product.find(function(err, product) 
    {
        if (err)
            res.send(err);
  
        res.json({ message: 'Product list!', data: product });
    });
};

// Find specific product
exports.findProduct = function(req, res) {

    Product.findById(req.params.product_id, function(err, product) 
    {
        if (err)
            res.send(err);
    
        res.json({ message: 'Product list!', data: product });
    });
};

// Update product
exports.updateProduct = function(req, res) {

    Product.findById(req.params.product_id, function(err, product) 
    {
        if (err)
            res.send(err);

        product.quantity = req.body.quantity;
    
        product.save(function(err) {
            if (err)
                res.send(err);
    
            res.json(product);
        });
    });
};

// Remove selected product
exports.deleteProduct = function(req, res) {

    Product.findByIdAndRemove(req.params.product_id, function(err) {
        if (err)
            res.send(err);
  
        res.json({ message: 'Product was removed!' });
    });
};