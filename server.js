var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var passport = require('passport');
var session = require('express-session');
var ejs = require('ejs'); // optional for http render view

var authController = require('./controllers/auth');
var clientController = require('./controllers/client');
var productController = require('./controllers/product');
var usersController = require('./controllers/users');
var oauthController = require('./controllers/oauth');

mongoose.connect('mongodb://localhost:27017/expressDb', { useMongoClient: true });

// Create application
var app = express();
app.use(bodyParser.urlencoded({
    extended: true
}));

// Set view engine to ejs this is optional for rendering into http format
app.set('view engine', 'ejs');

// Use express session support since OAuth2orize requires it
app.use(session({
    secret: '$2a$05$aAu3tBDsJYeyLSRY#',
    saveUninitialized: true,
    resave: true
}));

app.use(passport.initialize());

// Use port default 8000
var port = process.env.PORT || 8000;

// Route
var router = express.Router();

// http://localhost:8000/api
router.get('/', function(req, res) {
  res.json({ message: 'Welcome!' });
});

// Create endpoint handlers for /product
router.route('/product')
.post(authController.isAuthenticated, productController.postProduct)
.get(authController.isAuthenticated, productController.getProduct);

// Create endpoint handlers for /product/:product_id
router.route('/product/:product_id')
.get(authController.isAuthenticated, productController.findProduct)
.put(authController.isAuthenticated, productController.updateProduct)
.delete(authController.isAuthenticated, productController.deleteProduct);

// Create endpoint handlers for /users
router.route('/users')
.post(authController.isAuthenticated, usersController.postUsers)
.get(authController.isAuthenticated, usersController.getUsers);

// Create endpoint handlers for /client
router.route('/clients')
.post(authController.isAuthenticated, clientController.postClients)
.get(authController.isAuthenticated, clientController.getClients);

// Create endpoint handlers for oauth2 authorize
router.route('/oauth2/authorize')
.get(authController.isAuthenticated, oauthController.authorization)
.post(authController.isAuthenticated, oauthController.decision);

// Create endpoint handlers for oauth2 token
router.route('/oauth2/token')
.post(authController.isClientAuthenticated, oauthController.token);

// Register prefix api
app.use('/api', router);

// Start the server
app.listen(port);
console.log('Application running in ' + port);